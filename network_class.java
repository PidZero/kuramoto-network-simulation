import java.util.ArrayList;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.FileWriter;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class network_class{
    public double meanDegree;
    public ArrayList<Integer> test;
    public ArrayList<ArrayList < Double >> adjacency = new ArrayList< ArrayList < Double > >();

    // the random number generator is of the threadLocalRandom type
    // https://www.javamex.com/tutorials/random_numbers/e
    private Random rng = ThreadLocalRandom.current();
    // The number of nodes is N
    private int N = 0;
    // The infection probability is p
    private double p= 0.;
    // mean Distance between nodes
    private double meanDistance = 0.;
    // Diameter i.e. maximum distance between nodes
    private int maxDistance = 0;

    public ArrayList<ArrayList<Integer>> adList = new ArrayList<ArrayList<Integer>>();
    public void printRandomInt(){
        System.out.println(rng.nextInt(9));
    }
    public void printRandomDouble(){
        System.out.println(rng.nextDouble());
    }

    // create an undirected Price model
    public void initNetUndirectedPrice(int arg_size, int c, int a){
        N = arg_size;
        double r = (double)c/((double)c + (double)a);
        ArrayList < Integer > targetList = new ArrayList < Integer >();
        for(int ii = 0; ii < N; ii ++){
            adjacency.add(new ArrayList<Double>());
            for(ArrayList<Double> adj: adjacency){
                while(adj.size() < N){
                    adj.add(0.);
                }
            }
        }


        targetList.add(0);
        int k;
        for(int ii = 0; ii < N; ii++){
            for(int jj = 0; jj < c; jj++){
                if((ii!=0 || jj!=0) && rng.nextDouble() < r){
                    k = ii;
                    while(k == ii){
                        k = targetList.get(rng.nextInt(targetList.size()));
                    }
                    adjacency.get(ii).set(k, 1.);
                    adjacency.get(k).set(ii, 1.);
                    targetList.add(ii);
                    targetList.add(k);
                }else{
                    k = ii;
                    while(k == ii){
                        k = rng.nextInt(N-1);
                    }
                    adjacency.get(ii).set(k, 1.);
                    adjacency.get(k).set(ii, 1.);
                    targetList.add(ii);
                    targetList.add(k);
                }
            }
        }

            makeList();
    }


    // print the adjacency matrix to a file
    public void printAdjacency(String arg_filename){
        try{
            FileWriter writer = new FileWriter(arg_filename);
            // Gephi expects row and column labels 
            // print column labels
            for(int ii = 0; ii < N; ii++){
                writer.write("\t" + ii);
            }
            writer.write(System.lineSeparator());
            int row=0;
            for(ArrayList al: adjacency){
                writer.write(""+row);
                for(int ii = 0; ii < al.size(); ii++){
                    writer.write("\t" + al.get(ii).toString());
                }
                writer.write(System.lineSeparator());
                row++;
            }
            writer.close();
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }


    // create an adjacency list from the adjacency matrix
    private void makeList(){
        adList.clear();
        ArrayList<Integer> help = new ArrayList<Integer>();
        for(int ii = 0; ii < N; ii++){
            help.clear();
            for(int jj = 0; jj < N; jj++){
                if(adjacency.get(ii).get(jj).doubleValue()>0.){
                    help.add(jj);
                }
            }
            adList.add(help);
        }
    }



}

