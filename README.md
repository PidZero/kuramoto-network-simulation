# Kuramoto Network Simulation

A simple set of simulation tools for the Kuramoto model of synchronization

In the terminal under linux compile with
        javac *.java

And run with
        java mainClass
