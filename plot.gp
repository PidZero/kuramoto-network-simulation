set terminal png
set output "oscillators.png"
set xrange [0: 60]
plot for [i=1:10] "data.dat" u 0:i w l lw 2 title "Osc. ".i
