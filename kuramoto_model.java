import java.awt.*;
import javax.swing.*;
import javax.swing.*;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.ArrayList;
import java.lang.Math;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JFrame;

class kuramoto_model{

    // method for waiting milliseconds
    public static void wait(int ms)
    {
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }



    // Constructor sets up the network and the oscillators
    kuramoto_model(int arg_size, int arg_a, int arg_c, double arg_K){
        net.initNetUndirectedPrice(arg_size, arg_a, arg_c);
        N = arg_size;
        K = arg_K;
        // For Testing, set up initial angles randomly around the circle
        for(int ii = 0; ii < N; ii++){
            ksystem.add(new kuramoto_oscillator(1., rng.nextDouble()*2.*Math.PI));
        }
    }

    // advance time by one unit step according to Kuramoto:
    // dTheta_i/dt = omega_i + K/N sum_j sin(theta_j - theta_i)
    // dt/t = 0.1
    public void timestep(){
        double[] dtheta = new double[N];
        double h; 
        for(int ii = 0; ii < N; ii ++){
            h = 0.; 
            for(Object elem : net.adList.get(ii)){
                h+= Math.sin(ksystem.get((Integer)elem).theta - ksystem.get(ii).theta);
            }
            dtheta[ii] = .1*(ksystem.get(ii).omega + K/(double)N * h);
        }
        for(int ii = 0; ii < N; ii++){
            ksystem.get(ii).theta = (ksystem.get(ii).theta + dtheta[ii])%(2.*Math.PI);
        }
    }

    // print the current state to the system.out
    public void printCurrentState(){
        for(kuramoto_oscillator elem : ksystem){
            System.out.print(Math.sin(elem.theta) + "\t");
        }
        System.out.print("\n");
    }

    // Print the adjacency matrix to a file
    public void printAdjacency(){
        net.printAdjacency("adj.csv");
    }

    // the frame is needed to show the panel on which the state is drawn
    public JFrame myFrame = new MyFrame("Oscillators", this.ksystem);

    // the dimensions of the frame 
    private Dimension size = new Dimension(800, 800);

    // The panel is placed on the frame
    private MyPanel panel = new MyPanel(this.ksystem, size);

    // Initialize the frame for graphic display
    public void initShow(){
        myFrame.add(panel);
    }

    // show the current state of the oscillators
    public void showOscillators(){
        // update the panel so it shows the current position of the oscillators
        panel.updateOsc(ksystem);
        // repaint the panel
        panel.repaint();
    }



    // system will contain all oscillators
    private ArrayList <kuramoto_oscillator> ksystem = new ArrayList<kuramoto_oscillator>();

    // the network describes the coupling
    private network_class net = new network_class();

    // N is the number of oscillators
    public int N = 0;

    // coupling constant
    public double K = .3;

    // the random number generator is of the threadLocalRandom type
    // https://www.javamex.com/tutorials/random_numbers/e
    private Random rng = ThreadLocalRandom.current();

}

// The kiramoto oscillators are the builing blocks of the synchronization system
class kuramoto_oscillator{
    // default constructor
    kuramoto_oscillator(){
        omega = 1.;
        theta = 0.;
    }

    // constructor to set omega and theta
    kuramoto_oscillator(double arg_omega, double arg_theta){
        omega = arg_omega;
        theta = arg_theta;
    }

    // omega is the frequency (in rad) of the oscillator
    public double omega;

    // theta is the position of the oscillator
    public double theta;
}


class MyFrame extends JFrame{

    public MyPanel panel;

    MyFrame(String name, ArrayList<kuramoto_oscillator> oscList){

        osc = oscList;
        Dimension size = new Dimension(800, 800);
        this.setMaximumSize(size);
        this.setMinimumSize(size);
        this.setPreferredSize(size);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public ArrayList<kuramoto_oscillator> osc;
}



class MyPanel extends JPanel{

    //Image image;
    MyPanel(ArrayList<kuramoto_oscillator> arg_oscList, Dimension size){
        oscList = arg_oscList;
        this.setSize(size);
        xDim = size.width;
        yDim = size.height;
        centerX = (double)xDim/2.;
        centerY = (double)xDim/2.;
        R=0.4*(double)(xDim);
    }

    private ArrayList <kuramoto_oscillator> oscList;
    public int xDim = 300;
    public int yDim = 300;
    private double centerX;
    private double centerY;
    private double R=0.8*(double)(xDim);

    public void updateOsc(ArrayList<kuramoto_oscillator> osc){
        oscList = osc;
    }

    public void paint(Graphics g) {

        Graphics2D g2D = (Graphics2D) g;
        g2D.clearRect(0, 0, getWidth(), getHeight());

        g2D.setPaint(Color.blue);
        g2D.setStroke(new BasicStroke(5));
        g2D.drawOval((int)(0.1*(double)xDim), (int)(0.1*(double)yDim), (int)(0.8*(double)xDim), (int)(0.8*(double)yDim));
        g2D.setPaint(Color.pink);
        for(kuramoto_oscillator elem : oscList){
            g2D.drawLine((int)centerX, (int)centerY, (int)(centerX+R*Math.cos(elem.theta)), (int)(centerY+R*Math.sin(elem.theta)));
        }
    }
}
